# Changes to IO Processing
+ Refactored out the processing of association information when reading in attribute::Definitions so it can be overridden
+ Implemented missing support for V1/V2 association information for attribute::Definition
